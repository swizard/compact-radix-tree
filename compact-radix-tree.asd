;;; -*- Very compact, serializable and optimized for lookup perfomance radix tree implementation. -*-

(defpackage #:compact-radix-tree-asd
  (:use :cl :asdf))

(in-package #:compact-radix-tree-asd)

(defsystem compact-radix-tree
  :name "compact-radix-tree"
  :version "0.1"
  :author "swizard"
  :licence "BSD"
  :description "Very compact, serializable and optimized for lookup perfomance radix tree implementation."
  :depends-on (:iterate :bitfield-schema)
  :components ((:file "package")
               (:file "internal" :depends-on ("package"))
               (:file "public" :depends-on ("internal"))))
