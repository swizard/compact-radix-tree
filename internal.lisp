
(in-package :compact-radix-tree)

(defconstant +crt-magic+ #x049687f4)
(defconstant +crt-major+ 1)
(defconstant +crt-minor+ 1)

(defclass radix-tree ()
  ((data :initarg :data
         :accessor data)
   (alphabet-size :initarg :alphabet-size
                  :reader alphabet-size)
   (char-id-bits :initarg :char-id-bits
                 :reader char-id-bits)
   (char->id :initarg :char->id
             :reader char->id)
   (id->char :initarg :id->char
             :reader id->char)
   (values-count :initarg :values-count
                 :reader values-count)
   (value-id-bits :initarg :value-id-bits
                  :reader value-id-bits)
   (value->id :initarg :value->id
              :reader value->id)
   (id->value :initarg :id->value
              :reader id->value)))

(defbitfield-schema tree-header ()
  (magic :width 32)
  (major :width 8)
  (minor :width 8)
  (char-bits :width 5)
  (value-bits :width 7)
  (total-size :width 64))

(defun initialize-tree (tree data-provider)
  (declare (optimize (debug 3)))
  (declare (type (simple-bit-vector *) tree)
           (type function data-provider))
  (let ((char-index (make-hash-table))
        (value-index (make-hash-table))
        (offset 0)
        (data nil))
    (iter outer
          (declare (declare-variables))
          (multiple-value-bind (finish-p key value)
              (funcall data-provider)
            (until finish-p)
            (assert (and key (stringp key)))
            (iter (declare (declare-variables))
                  (for char in-string (the string key))
                  (for char-code = (char-code char))
                  (in outer (maximize char-code into max-char-value))
                  (unless (gethash char-code char-index)
                    (setf (gethash char-code char-index) t)))
            (when value
              (assert (numberp value))
              (maximize value into max-value)
              (unless (gethash value value-index)
                (setf (gethash value value-index) t)))
            (collect (cons key value) into collected-data))
          (finally
           (setf data collected-data)
           (let ((char-bits (integer-length (1+ max-char-value)))
                 (value-bits (integer-length (1+ (or max-value 0)))))
             (setf tree (make-tree-header tree
                                          :magic +crt-magic+
                                          :major +crt-major+
                                          :minor +crt-minor+
                                          :char-bits char-bits
                                          :value-bits value-bits
                                          :total-size 0))
             (incf offset (tree-header-size))
             (flet ((put/advance (value width)
                      (multiple-value-setq (tree offset)
                        (put-integer/grow value tree offset width))))
               (iter (for (char-code v) in-hashtable char-index)
                     (collect char-code into pool)
                     (finally (iter (for char-code in (sort pool #'<))
                                    (put/advance char-code char-bits))))
               (put/advance (1- (ash 1 char-bits)) char-bits)
               (iter (for (value v) in-hashtable value-index)
                     (collect value into pool)
                     (finally (iter (for value in (sort pool #'<))
                                    (put/advance value value-bits))))
               (put/advance (1- (ash 1 value-bits)) value-bits)))))
    (values tree offset data)))

(defun data-provider-list (list)
  (lambda ()
    (if (null list)
        t
        (let ((key+value (pop list)))
          (values nil
                  (if (consp key+value) (car key+value) key+value)
                  (when (consp key+value)
                    (cdr key+value)))))))

(defun make-radix-tree (tree)
  (let ((offset (tree-header-size))
        (char-bits (tree-header-char-bits tree))
        (alphabet-size 0)
        (char-id-bits 0)
        (char->id nil)
        (id->char nil)
        (value-bits (tree-header-value-bits tree))
        (values-count 0)
        (value-id-bits 0)
        (alphabet-offset 0)
        (values-offset 0)
        (value->id nil)
        (id->value nil))
    (flet ((next-bits (width)
             (let (value)
               (multiple-value-setq (value offset)
                 (get-integer tree offset width))
               value)))
      (setf alphabet-offset offset)
      (iter (for char-code = (next-bits char-bits))
            (until (= char-code (1- (ash 1 char-bits))))
            (collect (code-char char-code) into chars)
            (incf alphabet-size)
            (finally (setf char->id `(lambda (char tree)
                                       (declare (optimize (speed 3) (safety 0))
                                                (ignore tree))
                                       (case char
                                         ,@(iter (for ch in chars)
                                                 (for id from 0)
                                                 (collect (list ch id))))))))
      (setf values-offset offset)
      (iter (for value = (next-bits value-bits))
            (until (= value (1- (ash 1 value-bits))))
            (collect value into values)
            (incf values-count)))
    (setf char-id-bits (integer-length alphabet-size)
          value-id-bits (integer-length values-count)
          id->char `(lambda (id tree)
                      (declare (optimize (speed 3) (safety 0))
                               (type fixnum id)
                               (type (simple-bit-vector *) tree))
                      (when (<= 0 id ,(1- alphabet-size))
                        (code-char (get-integer/fixed tree (+ (* id ,char-bits) ,alphabet-offset) ,char-bits))))
          value->id `(lambda (value tree)
                       (declare (optimize (speed 3) (safety 0))
                                (type fixnum value)
                                (type (simple-bit-vector *) tree))
                       (iter (declare (declare-variables))
                             (with (the (integer 0 ,(1- values-count)) l) = 0)
                             (with (the (integer 0 ,(1- values-count)) h) = ,(1- values-count))
                             (while (<= l h))
                             (for m = (ash (+ l h) -1))
                             (for v = (get-integer/fixed tree (+ (* m ,value-bits) ,values-offset) ,value-bits))
                             (when (= v value)
                               (return m))
                             (if (< v value) (setf l (1+ m)) (setf h (1- m)))))
          id->value `(lambda (id tree)
                       (declare (optimize (speed 3) (safety 0))
                                (type fixnum id)
                                (type (simple-bit-vector *) tree))
                       (when (<= 0 id ,(1- values-count))
                         (get-integer/fixed tree (+ (* id ,value-bits) ,values-offset) ,value-bits))))
    (make-instance 'radix-tree
                   :data tree
                   :alphabet-size alphabet-size
                   :char-id-bits char-id-bits
                   :char->id (eval char->id)
                   :id->char (eval id->char)
                   :values-count values-count
                   :value-id-bits value-id-bits
                   :value->id (eval value->id)
                   :id->value (eval id->value))))

(defun build-radix-tree (data-provider)
  (let ((data (make-array 4096 :element-type 'bit)))
    (multiple-value-bind (data offset keys+values)
        (initialize-tree data data-provider)
      (declare (ignore keys+values))
      (let ((object (make-radix-tree data)))
        (let ((buffer (make-array offset :element-type 'bit)))
          (replace buffer data)
          (set-tree-header-total-size buffer offset)
          (setf (data object) buffer)
        object)))))

